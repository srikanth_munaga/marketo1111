var Marketo = require('node-marketo-rest');
var moment = require('moment');
var fg = require('../loginc.js/create');
var csv = require('fast-csv');
var fs = require('fs');
var ws = fs.createWriteStream('test22.csv');

var marketo_key = new Marketo({
    endpoint: 'https://937-WRZ-618.mktorest.com/rest',
    identity: 'https://937-WRZ-618.mktorest.com/identity',
    clientId: '0cf590bd-8a7c-4b74-a055-7c5b8eedaa67',
    clientSecret: 'awOVtVYWfIWQFaBB4u8m3gvrFWMZ9TmD'
  });

var options = {
    delimiter: ',',
    encoding: 'utf8',
    log: true,
    objName: false,
    parse: true,
    stream: false
};

marketo_key.bulkActivityExtract.get(
    {createdAt: 
        { 
            startAt:  moment().subtract(1,'days').format("YYYY-MM-DDT00:00:00:000") , 
            endAt: moment().format("YYYY-MM-DDT00:00:00:000") 
        },
        activityTypeIds:[6,7,8,9,10,11] 
    })
    .then(function(data) {
      // export is ready
      let exportId = data.result[0].exportId;
      console.log('export id is ' + exportId);
      return marketo_key.bulkActivityExtract.file(exportId);
    })
    .then(function(file) {
      // do something with file
    }); 
